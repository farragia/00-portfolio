var BoxOpened = "";
var ImgOpened = "";
var ImgFound = 0;
var replay = false;
var clicked = false;
var Source = "#boxcard";
var skor = [{name:"skor", time:"99:99:99"},
            {name:"skor2", time:"99:99:99"},
            {name:"skor3", time:"99:99:99"},
            {name:"skor4", time:"99:99:99"},
            {name:"skor5", time:"99:99:99"}];

var ImgSource = [
  // "file:///D:/KULIAH/Akademis/PPW/tugas1/src/images/awan.jpg",
  // "file:///D:/KULIAH/Akademis/PPW/tugas1/src/images/hati.jpg",
  // "file:///D:/KULIAH/Akademis/PPW/tugas1/src/images/segitiga.jpg",
  // "file:///D:/KULIAH/Akademis/PPW/tugas1/src/images/bulat.jpg",
  // "file:///D:/KULIAH/Akademis/PPW/tugas1/src/images/bunga.jpg",
  // "file:///D:/KULIAH/Akademis/PPW/tugas1/src/images/spade.jpg",
  // "file:///D:/KULIAH/Akademis/PPW/tugas1/src/images/clover.jpg",
  // "file:///D:/KULIAH/Akademis/PPW/tugas1/src/images/bintang.jpg"
  "src/images/awan.jpg",
  "src/images/hati.jpg",
  "src/images/segitiga.jpg",
  "src/images/bulat.jpg",
  "src/images/bunga.jpg",
  "src/images/spade.jpg",
  "src/images/clover.jpg",
  "src/images/bintang.jpg"
];

function RandomFunction(MaxValue, MinValue) {
    return Math.round(Math.random() * (MaxValue - MinValue) + MinValue);
  }
  
function ShuffleImages() {
  var ImgAll = $(Source).children();
  var ImgThis = $(Source + " div:first-child");
  var ImgArr = new Array();

  for (var i = 0; i < ImgAll.length; i++) {
    ImgArr[i] = $("#" + ImgThis.attr("id") + " img").attr("src");
    ImgThis = ImgThis.next();
  }
  
    ImgThis = $(Source + " div:first-child");
  
  for (var z = 0; z < ImgAll.length; z++) {
  var RandomNumber = RandomFunction(0, ImgArr.length - 1);

    $("#" + ImgThis.attr("id") + " img").attr("src", ImgArr[RandomNumber]);
    ImgArr.splice(RandomNumber, 1);
    ImgThis = ImgThis.next();
  }
}

function ResetGame() {
  ShuffleImages();
  $(Source + " div img").hide();
  $(Source + " div").css("visibility", "visible");
  $("#success").remove();
  BoxOpened = "";
  ImgOpened = "";
  ImgFound = 0;
  stopwatch.stop();
  return false;
}

function OpenCard() {
  if (clicked) {
    var id = $(this).attr("id");

  if ($("#" + id + " img").is(":hidden")) {
    $(Source + " div").unbind("click", OpenCard);
  
    $("#" + id + " img").fadeIn('fast');

    if (ImgOpened == "") {
      BoxOpened = id;
      ImgOpened = $("#" + id + " img").attr("src");
      setTimeout(function() {
        $(Source + " div").bind("click", OpenCard)
      }, 300);
    } else {
      CurrentOpened = $("#" + id + " img").attr("src");
      if (ImgOpened != CurrentOpened) {
        setTimeout(function() {
          $("#" + id + " img").slideUp('fast');
          $("#" + BoxOpened + " img").slideUp('fast');
          BoxOpened = "";
          ImgOpened = "";
        }, 400);
      } else {
        ImgFound++;
        BoxOpened = "";
        ImgOpened = "";
      }
      setTimeout(function() {
        $(Source + " div").bind("click", OpenCard)
      }, 400);
    }

    if(ImgFound == 8){
      var dummTime = "" + pad0(stopwatch.times[0], 2) + ":" + pad0(stopwatch.times[1], 2) + ":" + pad0(Math.floor(stopwatch.times[2]), 2);
      stopwatch.stop();
      inputScore(sessionStorage.getItem('username'), dummTime);
      showScore();
    }
  
  }
  } else {
    alert("click start!");
  }
}

$(function() {

for (var y = 1; y < 3 ; y++) {
  $.each(ImgSource, function(i, val) {
    $(Source).append("<div id=card" + y + i + "><img src=" + val + " />");
  });
}
  $(Source + " div").click(OpenCard);
  ShuffleImages();
});

var time = 0;

class Stopwatch {
    constructor(display, results) {
        this.running = false;
        this.display = display;
        this.results = results;
        this.laps = [];
        this.reset();
        this.print(this.times);
    }
    
    reset() {
        this.times = [ 0, 0, 0];
    }
    
    start() {
        clicked = true;
        this.reset();
        if (!this.time) this.time = performance.now();
        if (!this.running) {
            this.running = true;
            requestAnimationFrame(this.step.bind(this));
        }
    }
    
    lap() {
        let times = this.times;
        if (this.running) {
            this.reset();
        }
        let li = document.createElement('li');
        li.innerText = this.format(times);
        this.results.appendChild(li);
    }
    
    stop() {
        this.running = false;
        this.time = null;
    }

    restart() {
        if (!this.time) this.time = performance.now();
        if (!this.running) {
            this.running = true;
            requestAnimationFrame(this.step.bind(this));
        }
        this.reset();
    }
    
    clear() {
        clearChildren(this.results);
    }
    
    step(timestamp) {
        if (!this.running) return;
        this.calculate(timestamp);
        this.time = timestamp;
        this.print();
        requestAnimationFrame(this.step.bind(this));
    }
    
    calculate(timestamp) {
        var diff = timestamp - this.time;
        // Hundredths of a second are 100 ms
        this.times[2] += diff / 10;
        // Seconds are 100 hundredths of a second
        if (this.times[2] >= 100) {
            this.times[1] += 1;
            this.times[2] -= 100;
            time++;
        }
        // Minutes are 60 seconds
        if (this.times[1] >= 60) {
            this.times[0] += 1;
            this.times[1] -= 60;
        }
    }
    
    print() {
        this.display.innerText = this.format(this.times);
    }
    
    format(times) {
        return `\
${pad0(times[0], 2)}:\
${pad0(times[1], 2)}:\
${pad0(Math.floor(times[2]), 2)}`;
    }
}

function pad0(value, count) {
    var result = value.toString();
    for (; result.length < count; --count)
        result = '0' + result;
    return result;
}

function clearChildren(node) {
    while (node.lastChild)
        node.removeChild(node.lastChild);
}

let stopwatch = new Stopwatch(document.querySelector('#show'),document.querySelector('.results'));

function showScore(){
  if(JSON.parse(localStorage.getItem('skor')) !== null){
    var a = JSON.parse(localStorage.getItem('skor'));
  }
  else{
    var a = skor;    
  }
  localStorage.setItem('skor', JSON.stringify(a));
  a.sort(function(a,b){
    return a['time'].localeCompare(b['time']); 
  });
  var out = '<tr><td>nama</td><td>time</td></tr>';
  for (var i = 0; i < 5; i++){
    if(a[i].time == "99:99:99")
      out += "<td>"+a[i].name + "</td>" + "<td>" + "--:--:--" + "</td></tr><tr>";
    else
      out += "<td>"+a[i].name + "</td>" + "<td>" + a[i].time + "</td></tr><tr>";
  }
   $("#table-scor").html(out);
   console.log(a);
}

function inputScore(a,b){
  var i = JSON.parse(localStorage.getItem("skor"));
  i.push({'name':a, 'time':b});
  localStorage.setItem("skor", JSON.stringify(i)); 
}

$(document).ready(function(){
  showScore();
  document.getElementById("user").innerHTML = sessionStorage.getItem("username");
})